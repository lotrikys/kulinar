package ua.org.lotrik.kulinar.Views;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import ua.org.lotrik.kulinar.R;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    MainFragment mainFragment;
    CardFragment cardFragment;

    DrawerLayout drawerLayout;

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    NavigationView navigationView;
    public Toolbar mToolbar;
    ActionBarDrawerToggle mDrawerToggle;
    FrameLayout frameLayout;

    TextView toolbarTitle;

    String[] list;

    CharSequence mTitle;
    CharSequence mDrawerTitle;

    ImageView main_back;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        list = getResources().getStringArray(R.array.food_categories);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        frameLayout = (FrameLayout) findViewById(R.id.frame);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        main_back = (ImageView) findViewById(R.id.main_back);

        setSupportActionBar(mToolbar);

        setTitle(getString(R.string.main_str));

        fragmentManager = getSupportFragmentManager();

        if (mainFragment == null) mainFragment = new MainFragment();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame, mainFragment).commit();

        main_back.setBackgroundResource(R.drawable.main_back);

        mTitle = mDrawerTitle = getTitle();
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                null,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

        };

        drawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_main);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        toolbarTitle.setText(mTitle);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        if (item.getItemId() != R.id.nav_about)
            setTitle(item.getTitle());

        if (item.getItemId() != R.id.nav_main && item.getItemId() != R.id.nav_about) {
            mToolbar.setBackgroundResource(R.drawable.actionbar);
        }


        if (item.getItemId() == R.id.nav_main) {

            fragmentManager.beginTransaction().replace(R.id.frame, mainFragment).commit();
            navigationView.setCheckedItem(R.id.nav_main);
            drawerLayout.closeDrawer(navigationView);

            main_back.setBackgroundResource(R.drawable.main_back);

        } else if (item.getItemId() == R.id.nav_favorite) {

            navigationView.setCheckedItem(R.id.nav_favorite);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(1);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(1);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_first_dish) {

            navigationView.setCheckedItem(R.id.nav_first_dish);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(2);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(2);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_second_dish) {

            navigationView.setCheckedItem(R.id.nav_second_dish);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(3);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(3);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_salads) {

            navigationView.setCheckedItem(R.id.nav_salads);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(4);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(4);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_fish_dish) {

            navigationView.setCheckedItem(R.id.nav_fish_dish);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(5);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(5);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_meet_dish) {

            navigationView.setCheckedItem(R.id.nav_meet_dish);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(6);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(6);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_hot_snacks) {

            navigationView.setCheckedItem(R.id.nav_hot_snacks);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(7);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(7);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_desserts) {

            navigationView.setCheckedItem(R.id.nav_desserts);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(8);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(8);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_drinks) {

            navigationView.setCheckedItem(R.id.nav_drinks);

            main_back.setBackgroundResource(R.drawable.main_background);

            if (cardFragment == null) {
                cardFragment = new CardFragment();
                cardFragment.setId(9);
            } else {
                cardFragment.mAdapter.deleteData();
                cardFragment.setData(9);
            }
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, cardFragment).commit();
            drawerLayout.closeDrawer(navigationView);

        } else if (item.getItemId() == R.id.nav_about) {

            drawerLayout.closeDrawer(navigationView);

            Intent intent = new Intent(this, About.class);
            startActivity(intent);
        }

        return true;
    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {

//            android.os.Process.killProcess(android.os.Process.myPid());
            finish();
            super.onBackPressed();
            return;
        }

        if (drawerLayout.isDrawerOpen(navigationView)) {

            drawerLayout.closeDrawer(navigationView);

        } else {

            this.doubleBackToExitPressedOnce = true;

            if (mainFragment.getContext() != null) {

                Toast.makeText(mainFragment.getContext(), "Для выхода из приложения нажмите кнопку \"Назад\"" +
                        " еще раз", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(cardFragment.getContext(), "Для выхода из приложения нажмите кнопку \"Назад\"" +
                        " еще раз", Toast.LENGTH_SHORT).show();
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
//        getWindow().getDecorView().setSystemUiVisibility(uiOptions);
    }

}
