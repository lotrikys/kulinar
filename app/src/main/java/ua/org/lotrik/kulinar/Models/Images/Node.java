package ua.org.lotrik.kulinar.Models.Images;

/**
 * Created by lotrik on 16.05.16.
 */
public class Node {
    private String timestamp;

    private String title;

    private String nid;

    private String[] first_img;

    private String[] img;

    private String[] img_fish;

    private String[] meets_img;

    private String[] snacks_img;

    private String[] desserts_img;

    private String[] salads_img;

    private String[] drinks_img;

    private String tnid;

    private String type;

    private String url;

    public String[] getFirst_img() {
        return first_img;
    }

    public void setFirst_img(String[] first_img) {
        this.first_img = first_img;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {
        this.img = img;
    }

    public String[] getImg_fish() {
        return img_fish;
    }

    public void setImg_fish(String[] img_fish) {
        this.img_fish = img_fish;
    }

    public String[] getMeets_img() {
        return meets_img;
    }

    public void setMeets_img(String[] meets_img) {
        this.meets_img = meets_img;
    }

    public String[] getDesserts_img() {
        return desserts_img;
    }

    public void setDesserts_img(String[] desserts_img) {
        this.desserts_img = desserts_img;
    }

    public String[] getSalads_img() {
        return salads_img;
    }

    public void setSalads_img(String[] salads_img) {
        this.salads_img = salads_img;
    }

    public String[] getDrinks_img() {
        return drinks_img;
    }

    public void setDrinks_img(String[] drinks_img) {
        this.drinks_img = drinks_img;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String[] getSnacks_img() {
        return snacks_img;
    }

    public void setSnacks_img(String[] snacks_img) {
        this.snacks_img = snacks_img;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
