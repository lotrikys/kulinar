package ua.org.lotrik.kulinar.Models.Comments;

/**
 * Created by lotrik on 02.06.16.
 */
public class Comment_body
{
    private String value;

    private String format;

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getFormat ()
    {
        return format;
    }

    public void setFormat (String format)
    {
        this.format = format;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [value = "+value+", format = "+format+"]";
    }
}
