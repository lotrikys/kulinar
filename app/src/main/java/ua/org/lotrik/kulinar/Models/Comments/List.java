package ua.org.lotrik.kulinar.Models.Comments;

/**
 * Created by lotrik on 02.06.16.
 */
public class List {
    private String edit_url;

    private String mail;

    private Comment_body comment_body;

    private String status;

    private String subject;

    private String hostname;

    private String cid;

    private String homepage;

    private String url;

    private String created;

    private String name;

    public String getEdit_url ()
    {
        return edit_url;
    }

    public void setEdit_url (String edit_url)
    {
        this.edit_url = edit_url;
    }

    public String getMail ()
    {
        return mail;
    }

    public void setMail (String mail)
    {
        this.mail = mail;
    }

    public Comment_body getComment_body ()
    {
        return comment_body;
    }

    public void setComment_body (Comment_body comment_body)
    {
        this.comment_body = comment_body;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getSubject ()
    {
        return subject;
    }

    public void setSubject (String subject)
    {
        this.subject = subject;
    }

    public String getHostname ()
    {
        return hostname;
    }

    public void setHostname (String hostname)
    {
        this.hostname = hostname;
    }

    public String getCid ()
    {
        return cid;
    }

    public void setCid (String cid)
    {
        this.cid = cid;
    }

    public String getHomepage ()
    {
        return homepage;
    }

    public void setHomepage (String homepage)
    {
        this.homepage = homepage;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getCreated ()
    {
        return created;
    }

    public void setCreated (String created)
    {
        this.created = created;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [edit_url = "+edit_url+", mail = "+mail+", comment_body = "+comment_body+"" +
                ", status = "+status+", subject = "+subject+", hostname = "+hostname+", cid = "+cid+"" +
                ", homepage = "+homepage+", url = "+url+", created = "+created+", name = "+name+"]";
    }
}
