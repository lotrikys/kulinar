package ua.org.lotrik.kulinar.Views;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.WindowManager;
import android.widget.TextView;

import ua.org.lotrik.kulinar.R;

public class About extends AppCompatActivity {

    TextView textView4;
    TextView textView5;
    TextView textView6;
    TextView textView7;
    TextView textView8;
    TextView textView9;
    TextView textView10;

    TextView toolbarTitle;

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_about);

        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView7 = (TextView) findViewById(R.id.textView7);
        textView8 = (TextView) findViewById(R.id.textView8);
        textView9 = (TextView) findViewById(R.id.textView9);
        textView10 = (TextView) findViewById(R.id.textView10);

        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        toolbarTitle.setText(R.string.about);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorToolBarText));

        mToolbar.setBackgroundResource(R.drawable.actionbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView4.setText("Разработка дизайна:");
        textView4.setMovementMethod(LinkMovementMethod.getInstance());
        textView7.setText("Матвиенко Юлия");
        textView8.setText(Html.fromHtml("<a href=https://vk.com/yulia_matvienko_design>Страница Вконтакте</a>"));
        textView8.setMovementMethod(LinkMovementMethod.getInstance());
        textView9.setText("Skype: shinshilla0601");
        textView10.setText(Html.fromHtml("Email: <a href=\"mailto:shinshilla24@gmail.com\">shinshilla24@gmail.com</a>"));
        textView10.setMovementMethod(LinkMovementMethod.getInstance());

        textView5.setText(Html.fromHtml("Контакты: <a href=\"mailto:kulinar@lotrik.org.ua\">" +
                "kulinar@lotrik.org.ua</a>"));
        textView5.setMovementMethod(LinkMovementMethod.getInstance());

        textView6.setText(Html.fromHtml("Сайт: <a href=http://kulinar.pp.ua>kulinar.pp.ua</a>"));
        textView6.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }
}
