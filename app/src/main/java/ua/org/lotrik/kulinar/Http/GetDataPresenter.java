package ua.org.lotrik.kulinar.Http;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ua.org.lotrik.kulinar.Models.Comments.Comments;
import ua.org.lotrik.kulinar.Models.Images.Images;
import ua.org.lotrik.kulinar.Models.Recepts.Rss;
import ua.org.lotrik.kulinar.Views.Adapters.RecylcerAdapter;
import ua.org.lotrik.kulinar.Views.CardFragment;
import ua.org.lotrik.kulinar.Views.Adapters.ImageAdapter;
import ua.org.lotrik.kulinar.Views.ShowRecept;

/**
 * Created by student on 21.12.2015.
 */
public class GetDataPresenter {

    CardFragment cardFragment;
    Context context;

    int id;

    ApiService apiService;

    RecylcerAdapter recylcerAdapter = new RecylcerAdapter();

    public GetDataPresenter() {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://kulinar.pp.ua").build();
        apiService = restAdapter.create(ApiService.class);
    }

    public void getData(CardFragment cardFragment, Context context) {
        this.cardFragment = cardFragment;
        this.context = context;

    }

    public void getRecept(final int id) {

        this.id = id;

        apiService.getRecept(cardFragment.type, new Callback<Rss>() {

            @Override
            public void success(Rss rss, Response response) {

                cardFragment.progressDialog.dismiss();

                cardFragment.swipeRefreshLayout.setRefreshing(false);

                cardFragment.mAdapter.addData(rss, id);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("tag-e", error.getMessage());

                cardFragment.progressDialog.dismiss();

                cardFragment.swipeRefreshLayout.setRefreshing(false);

                Toast.makeText(context, "Отсутствует Интернет...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getImages(final int nid, final ShowRecept showRecept, final String type, final int size) {

        apiService.getImages(nid, new Callback<Images>() {
            @Override
            public void success(Images images, Response response) {

                ImageAdapter imageAdapter = new ImageAdapter(showRecept.getApplicationContext());

                switch (type) {

                    case "first_dish": {
                        imageAdapter.setData(images.getNode().getFirst_img(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "second_dish": {
                        imageAdapter.setData(images.getNode().getImg(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "salads": {
                        imageAdapter.setData(images.getNode().getSalads_img(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "fish_dish": {
                        imageAdapter.setData(images.getNode().getImg_fish(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "meet_dish": {
                        imageAdapter.setData(images.getNode().getMeets_img(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "hot_snacks": {
                        imageAdapter.setData(images.getNode().getSnacks_img(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "desserts": {
                        imageAdapter.setData(images.getNode().getDesserts_img(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                    case "drinks": {
                        imageAdapter.setData(images.getNode().getDrinks_img(), size);
                        showRecept.gridView.setAdapter(imageAdapter);
                        break;
                    }
                }


            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public RecylcerAdapter getComments (int nid) {

        apiService.getComment(nid, new Callback<Comments>() {
            @Override
            public void success(Comments comments, Response response) {

                recylcerAdapter.setData(comments);

            }

            @Override
            public void failure(RetrofitError error) {

                Log.d("tag11", error.getMessage());
            }
        });

        return recylcerAdapter;
    }
}
