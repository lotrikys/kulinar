package ua.org.lotrik.kulinar.Models.Images;

/**
 * Created by lotrik on 16.05.16.
 */
public class Images {
    private String message;

    private String cached;

    private String result;

    private Node node;

    private String count;

    private String gentime;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCached() {
        return cached;
    }

    public void setCached(String cached) {
        this.cached = cached;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getGentime() {
        return gentime;
    }

    public void setGentime(String gentime) {
        this.gentime = gentime;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", cached = " + cached + ", result = " + result + ", node = " + node + ", count = " + count + ", gentime = " + gentime + "]";
    }
}
