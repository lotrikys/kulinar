package ua.org.lotrik.kulinar.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ua.org.lotrik.kulinar.Models.Recepts.Nodes;
import ua.org.lotrik.kulinar.Models.Recepts.Rss;
import ua.org.lotrik.kulinar.R;
import ua.org.lotrik.kulinar.SQLite.ChoosenDB;
import ua.org.lotrik.kulinar.Views.ShowRecept;

/**
 * Created by lotrik on 30.11.2015.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private ArrayList<String> mDatasetImg;

    ArrayList<Nodes> items = new ArrayList<>();

    Context context;
    int id;
    Rss rss;

    private int lastPosition = -1;

    public static class ViewHolder extends RecyclerView.ViewHolder {


        TextView title;
        TextView anons;
        ImageView anons_img;
        CardView cardView;
        ImageView favorite;
        ImageView share;

        public ViewHolder(View v) {
            super(v);

            title = (TextView) v.findViewById(R.id.title);
            anons = (TextView) v.findViewById(R.id.anons);
            anons_img = (ImageView) v.findViewById(R.id.anons_img);
            cardView = (CardView) v.findViewById(R.id.card_view);
            favorite = (ImageView) v.findViewById(R.id.favorite);
            share = (ImageView) v.findViewById(R.id.share);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("tag1", "press");
                }
            });
        }

    }

    public CardAdapter(int id, Context context, ArrayList<String> mDatasetImg) {
        super();

        this.mDatasetImg = mDatasetImg;

        this.id = id;
        this.context = context;
    }

    public void addData(Rss rss, int id) {

        this.id = id;
        this.rss = rss;

        items = rss.getNodes();

        int j = 0;

        for (int i = 0; i < rss.getCount(); i++) {

            if (id == 1) {
                mDatasetImg.add(j, items.get(i).getImg_anons());
            } else if (id == 2) {
                mDatasetImg.add(j, items.get(i).getFirst_anons());
            } else if (id == 3) {
                mDatasetImg.add(j, items.get(i).getImg_anons());
            } else if (id == 4) {
                mDatasetImg.add(j, items.get(i).getSalads_anons());
            } else if (id == 5) {
                mDatasetImg.add(j, items.get(i).getImg_anons_fish());
            } else if (id == 6) {
                mDatasetImg.add(j, items.get(i).getMeets_anons());
            } else if (id == 7) {
                mDatasetImg.add(j, items.get(i).getSnacks_anons());
            } else if (id == 8) {
                mDatasetImg.add(j, items.get(i).getDesserts_anons());
            } else if (id == 9) {
                mDatasetImg.add(j, items.get(i).getDrinks_anons());
            }

            j++;
        }

        notifyDataSetChanged();
    }


    public void deleteData() {

        items.clear();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent,
                false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.title.setText(items.get(position).getTitle());
        holder.anons.setText(items.get(position).getBody().getSummary());

        setAnimation(holder.cardView, position);

        Glide.with(context).load(mDatasetImg.get(position)).into(holder.anons_img);

        if (id == 1) {
            holder.favorite.setBackgroundResource(R.drawable.favorites_remove);
        } else if (!new ChoosenDB(context).isPresentInDB(Integer.parseInt(items.get(position).getNid()))) {
            holder.favorite.setBackgroundResource(R.drawable.favorites_add);
        } else {
            holder.favorite.setBackgroundResource(R.drawable.favorites_added);
        }

        holder.share.setBackgroundResource(R.drawable.share);

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                StringBuilder shareText = new StringBuilder();
                shareText.append(items.get(position).getTitle().replaceAll("<(.)+?>", "")
                        .replaceAll("<(\n)+?>", ""));
                shareText.append(".\n\n");
                shareText.append(items.get(position).getBody().getSafe_value().replaceAll("<(.)+?>", "")
                        .replaceAll("<(\n)+?>", "").replaceAll(":", ":\n").replaceAll("ГОТ", "\nГОТ"));

                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, items.get(position).getTitle());
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareText.toString());
                context.startActivity(Intent.createChooser(sharingIntent, "Поделиться через"));
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri address = Uri.parse(items.get(position).getUrl());

//                Intent openlink = new Intent(Intent.ACTION_VIEW, address);

//                context.startActivity(openlink);

                Intent intent = new Intent(context, ShowRecept.class);
                intent.putExtra("descr", items.get(position).getBody().getSafe_value());
                intent.putExtra("link", items.get(position).getUrl());
                intent.putExtra("dish", items.get(position).getTitle());
                intent.putExtra("nid", items.get(position).getNid());
                intent.putExtra("type", items.get(position).getType());
                intent.putExtra("id", id);
                context.startActivity(intent);
            }
        });

        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Integer nid = Integer.parseInt(items.get(position).getNid());

                if (id == 1) {

                    new ChoosenDB(context).deleteFromDB(nid);

                    Toast.makeText(context, "Рецепт " + items.get(position).getTitle() + " удален из" +
                            " избранное", Toast.LENGTH_SHORT).show();

                    mDatasetImg.remove(position);
                    items.remove(position);

                } else if (new ChoosenDB(context).isPresentInDB(Integer.parseInt(items.get(position)
                        .getNid()))) {

                    new ChoosenDB(context).deleteFromDB(nid);

                    Toast.makeText(context, "Рецепт " + items.get(position).getTitle() + " удален из" +
                            " избранное", Toast.LENGTH_SHORT).show();
                } else {

                    new ChoosenDB(context).saveInDB(mDatasetImg.get(position),
                            items.get(position).getTitle(),
                            items.get(position).getBody().getSummary(),
                            items.get(position).getUrl(),
                            items.get(position).getBody().getSafe_value(),
                            nid);
                }

                notifyDataSetChanged();
            }
        });
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
//        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
