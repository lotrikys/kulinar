package ua.org.lotrik.kulinar.Views;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ua.org.lotrik.kulinar.Http.GetDataPresenter;
import ua.org.lotrik.kulinar.R;
import ua.org.lotrik.kulinar.SQLite.ChoosenDB;
import ua.org.lotrik.kulinar.Views.Adapters.CardAdapter;

/**
 * Created by lotrik on 30.11.2015.
 */
public class CardFragment extends Fragment {


    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    public CardAdapter mAdapter;
    Context context;
    public String type = "";

    public SwipeRefreshLayout swipeRefreshLayout;

    int id;

    public ProgressDialog progressDialog;

    boolean swipe = false;

    ArrayList<String> mDatasetImg = new ArrayList<>();

    ChoosenDB choosenDB;

    GetDataPresenter getDataPresenter;
    private View rootView;

    MainActivity mainActivity;

    public CardFragment() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setData(int id) {

        this.id = id;

        if (id == 1) {
            swipeRefreshLayout.setEnabled(false);
        } else {
            swipeRefreshLayout.setEnabled(true);
        }

        if (id != 1 && !swipe) {

            progressDialog.setMessage("Загрузка...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        swipe = false;

        setmAdapter();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.card_fragment, container, false);

        mainActivity = (MainActivity) getActivity();

        context = getContext();

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipe = true;

                mAdapter = new CardAdapter(id, context, mDatasetImg);

                setData(id);

            }
        });

        if (progressDialog == null) {

            progressDialog = new ProgressDialog(context);
        }

        mAdapter = new CardAdapter(id, context, mDatasetImg);

        setData(id);

        return rootView;
    }


    public void setmAdapter() {

        switch (id) {
            case 2: {
                type = "first_dish";
                break;
            }
            case 3: {
                type = "second_dish";
                break;
            }
            case 4: {
                type = "salads";
                break;
            }
            case 5: {
                type = "fish_dish";
                break;
            }
            case 6: {
                type = "meet_dish";
                break;
            }
            case 7: {
                type = "hot_snacks";
                break;
            }
            case 8: {
                type = "desserts";
                break;
            }
            case 9: {
                type = "drinks";
                break;
            }

        }

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_card_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        if (id == 1) {

            choosenDB = new ChoosenDB(context);
            choosenDB.getDataFromDB(this, id);

            swipeRefreshLayout.setRefreshing(false);

        } else {

            getDataPresenter = new GetDataPresenter();
            getDataPresenter.getData(this, context);
            getDataPresenter.getRecept(id);

        }
    }

}
