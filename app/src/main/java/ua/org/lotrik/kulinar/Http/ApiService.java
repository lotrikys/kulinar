package ua.org.lotrik.kulinar.Http;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import ua.org.lotrik.kulinar.Models.Comments.Comments;
import ua.org.lotrik.kulinar.Models.Images.Images;
import ua.org.lotrik.kulinar.Models.Recepts.Rss;

/**
 * Created by lotrik on 30.11.2015.
 */
public interface ApiService {

    @GET("/rest/list/{type}")
    void getRecept(@Path("type") String type, Callback<Rss> callback);

    @GET("/rest/node/{id}")
    void getImages(@Path("id") int id, Callback<Images> callback);

    @GET("/comment.json")
    void getComment(@Query("node") int nid, Callback<Comments> callback);
}
