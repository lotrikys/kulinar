package ua.org.lotrik.kulinar.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

import ua.org.lotrik.kulinar.Models.Recepts.Body;
import ua.org.lotrik.kulinar.Models.Recepts.Nodes;
import ua.org.lotrik.kulinar.Models.Recepts.Rss;
import ua.org.lotrik.kulinar.Views.CardFragment;

/**
 * Created by lotrik on 16.12.15.
 */
public class ChoosenDB {


    Context context;
    DBHelper dbHelper;
    SQLiteDatabase db;
    ContentValues cv;

    Rss rssDB;

    int id;

    public ChoosenDB(Context context) {

        this.context = context;
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        cv = new ContentValues();
        rssDB = new Rss();
    }


    public void saveInDB(String img,
                         String title,
                         String anons,
                         String link,
                         String descr,
                         int nid) {

        if (!isPresentInDB(nid)) {

            cv.put("nid", nid);
            cv.put("img", img);
            cv.put("title", title);
            cv.put("anons", anons);
            cv.put("link", link);
            cv.put("descr", descr);

            try {
                db.insert("choosen", null, cv);
                db.close();

                Toast.makeText(context, "Рецепт " + title + " добавлен в" +
                        " избранное", Toast.LENGTH_SHORT).show();

            } catch (SQLException e) {
                e.printStackTrace();


            }

        } else {

            Toast.makeText(context, "Рецепт " + title + " уже добавлен в" +
                    " избранное", Toast.LENGTH_SHORT).show();
        }
    }


    public void deleteFromDB(int nid) {

        db.delete("choosen", "nid=" + nid, null);
        db.close();
    }


    public void getDataFromDB(CardFragment cardFragment, int id) {

        this.id = id;

        int i = 0;

        rssDB = new Rss();

        ArrayList<Nodes> nodesArrayList = new ArrayList<>();
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query("choosen", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {

                Nodes nodes = new Nodes();
                Body body = new Body();

                body.setSummary(cursor.getString(cursor.getColumnIndex("anons")));
                body.setSafe_value(cursor.getString(cursor.getColumnIndex("descr")));
                nodes.setBody(body);
                nodes.setImg_anons(cursor.getString(cursor.getColumnIndex("img")));
                nodes.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                nodes.setUrl(cursor.getString(cursor.getColumnIndex("link")));
                nodes.setNid(cursor.getString(cursor.getColumnIndex("nid")));
                nodesArrayList.add(i, nodes);

                i++;

            } while (cursor.moveToNext());

            rssDB.setCount(i);
            rssDB.setNodes(nodesArrayList);

            cardFragment.mAdapter.addData(rssDB, id);

            db.close();
            cursor.close();
        } else {

            cardFragment.mAdapter.deleteData();
            Toast.makeText(context, "Нет рецептов добавленных в избранное",
                    Toast.LENGTH_SHORT).show();
        }
    }


    public boolean isPresentInDB(int nid) {

        Cursor cursor = db.query("choosen", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {

            do {
                if (nid == Integer.parseInt(cursor.getString(cursor.getColumnIndex("nid")))) {
                    db.close();
                    cursor.close();
                    return true;
                }
            } while (cursor.moveToNext());
        }
        return false;
    }
}
