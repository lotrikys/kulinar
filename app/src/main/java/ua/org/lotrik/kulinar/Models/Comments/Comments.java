package ua.org.lotrik.kulinar.Models.Comments;

/**
 * Created by lotrik on 02.06.16.
 */
public class Comments {

    private List[] list;

    public List[] getList ()
    {
        return list;
    }

    public void setList (List[] list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [list = "+list+"]";
    }
}
