package ua.org.lotrik.kulinar.Models.Recepts;

/**
 * Created by lotrik on 09.12.2015.
 */
public class Body {
    private String summary;

    private String safe_value;

    private String safe_summary;

    private String value;

    private String format;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSafe_value() {
        return safe_value;
    }

    public void setSafe_value(String safe_value) {
        this.safe_value = safe_value;
    }

    public String getSafe_summary() {
        return safe_summary;
    }

    public void setSafe_summary(String safe_summary) {
        this.safe_summary = safe_summary;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return "ClassPojo [summary = " + summary + ", safe_value = " + safe_value + ", safe_summary = " + safe_summary + ", value = " + value + ", format = " + format + "]";
    }
}
