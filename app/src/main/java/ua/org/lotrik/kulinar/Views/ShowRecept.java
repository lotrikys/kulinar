package ua.org.lotrik.kulinar.Views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import ua.org.lotrik.kulinar.Http.GetDataPresenter;
import ua.org.lotrik.kulinar.R;

public class ShowRecept extends AppCompatActivity {

    static String descr;
    String link;
    String dish;
    static int nid;

    static int id;

    Button openLink;

    TextView toolbarTitle;

    static HtmlTextView htmlTextView;

    DisplayMetrics metrics;

    static int size;

    Toolbar mToolbar;

    static String type;

    static ShowRecept showRecept;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    public static GridView gridView;

    static TabLayout tabLayout;

    public static RecyclerView mRecyclerView;

    static GetDataPresenter getDataPresenter = new GetDataPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_show_recept);

        openLink = (Button) findViewById(R.id.openLink);

        showRecept = this;

        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        size = metrics.widthPixels;

        size = (size - 30) / 3;

        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        Intent intent = getIntent();
        descr = intent.getStringExtra("descr");
        link = intent.getStringExtra("link");
        dish = intent.getStringExtra("dish");
        nid = Integer.parseInt(intent.getStringExtra("nid"));
        type = intent.getStringExtra("type");
        id = intent.getIntExtra("id", 0);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorToolBarText));
        mToolbar.setBackgroundResource(R.drawable.actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle.setText(dish);

        openLink.setAllCaps(false);

        openLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri address = Uri.parse(link);

                Intent openlink = new Intent(Intent.ACTION_VIEW, address);

                startActivity(openlink);
            }
        });

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //Log.d("tag11", "onCreate " + getArguments().getInt(ARG_SECTION_NUMBER));
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_show_recept, container, false);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

            gridView = (GridView) rootView.findViewById(R.id.gridView);
            gridView.setColumnWidth(size);

            mRecyclerView = (RecyclerView)rootView.findViewById(R.id.rv);
            mRecyclerView.setLayoutManager(mLayoutManager);

            if (id != 1) {
                getDataPresenter.getImages(nid, showRecept, type, size);
                mRecyclerView.setAdapter(getDataPresenter.getComments(nid));
            }

            htmlTextView = (HtmlTextView) rootView.findViewById(R.id.html_text);
            htmlTextView.setHtmlFromString(descr, new HtmlTextView.LocalImageGetter());

            if (id == 1){

                htmlTextView.setVisibility(rootView.VISIBLE);
                tabLayout.setVisibility(rootView.GONE);
            }

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {

                htmlTextView.setVisibility(rootView.VISIBLE);
            }

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {

                gridView.setVisibility(rootView.VISIBLE);

            }

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 3) {

                mRecyclerView.setVisibility(rootView.VISIBLE);

            }

            return rootView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            //Log.d("tag11", "onDestroy " + getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            if (id != 1) {
                return 3;
            } else {
                return 1;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Рецепт";
                case 1:
                    return "Фото";
                case 2:
                    return "Комментарии";
            }
            return null;
        }
    }

}
