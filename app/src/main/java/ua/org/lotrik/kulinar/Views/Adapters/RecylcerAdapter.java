package ua.org.lotrik.kulinar.Views.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import ua.org.lotrik.kulinar.Models.Comments.Comments;
import ua.org.lotrik.kulinar.R;

/**
 * Created by lotrik on 02.06.16.
 */
public class RecylcerAdapter extends RecyclerView.Adapter<RecylcerAdapter.ViewHolder>{

    private Comments comments;

    public void setData(Comments comments) {
        this.comments = comments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent,
                false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        Log.d("tag11", comments.getList().length + "");

        if (comments.getList().length != 0) {

            int timestamp = Integer.parseInt(comments.getList()[position].getCreated());

            Date time = new Date((long)timestamp * 1000);

            SimpleDateFormat mDateFormat = new SimpleDateFormat("d.MM.yyyy hh:mm");

            String author = comments.getList()[position].getName() + " написал:";
            String comment = comments.getList()[position].getComment_body().getValue().replaceAll("<(.)+?>", "") +
                    "\n" + mDateFormat.format(time);

            holder.mAuthor.setText(author);
            holder.mTextView.setText(comment);
        } else {
            holder.mAuthor.setVisibility(View.GONE);
            holder.mTextView.setText(R.string.no_comments);
        }
    }

    @Override
    public int getItemCount() {

        if (comments.getList().length != 0) {

            return comments.getList().length;
        }

        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;
        TextView mAuthor;

        public ViewHolder(View itemView) {
            super(itemView);

            mTextView = (TextView)itemView.findViewById(R.id.tv_recycler_item);
            mAuthor = (TextView)itemView.findViewById(R.id.author);
        }
    }
}
