package ua.org.lotrik.kulinar.Models.Recepts;


import java.util.ArrayList;

/**
 * Created by lotrik on 30.11.2015.
 */

public class Rss {

    private ArrayList<Nodes> nodes;

    private ArrayList<Nodes> nodesReverse = new ArrayList<>();

    private String message;

    private String cached;

    private String result;

    private int count;


    private String gentime;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCached() {
        return cached;
    }

    public void setCached(String cached) {
        this.cached = cached;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Nodes> getNodes() {

        int j = 0;
        for (int i = nodes.size() - 1; i >= 0; i--) {
            nodesReverse.add(j, nodes.get(i));
            j++;
        }

        return nodesReverse;
    }

    public void setNodes(ArrayList<Nodes> nodes) {
        this.nodes = nodes;
    }

    public String getGentime() {
        return gentime;
    }

    public void setGentime(String gentime) {
        this.gentime = gentime;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", cached = " + cached + ", result = " + result + ", count = " + count + ", nodes = " + nodes + ", gentime = " + gentime + "]";
    }
}