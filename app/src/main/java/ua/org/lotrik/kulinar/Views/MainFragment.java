package ua.org.lotrik.kulinar.Views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.org.lotrik.kulinar.R;

/**
 * Created by lotrik on 09.12.15.
 */
public class MainFragment extends Fragment {

    TextView mainText;

    MainActivity mainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.main_fragment, container, false);

        mainText = (TextView) rootView.findViewById(R.id.mainText);
        mainText.setText(R.string.main_text);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mainActivity.mToolbar != null) mainActivity.mToolbar.setBackgroundResource(0);
    }
}
