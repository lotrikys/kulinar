package ua.org.lotrik.kulinar.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import ua.org.lotrik.kulinar.R;
import ua.org.lotrik.kulinar.Views.FullScreenImage;

/**
 * Created by lotrik on 16.05.16.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    String[] images;

    int size;

    public ImageAdapter(Context c) {
        mContext = c;

    }

    public void setData(String[] images, int size) {
        this.images = images;
        this.size = size;
    }

    public int getCount() {
        return images.length;
    }

    public Object getItem(int position) {
        return images[position];
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(size, size));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        Glide.with(mContext).load(images[position])
                .placeholder(R.color.grey_light)
                .into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FullScreenImage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", position);
                intent.putExtra("images", images);
                mContext.startActivity(intent);
            }
        });
        return imageView;
    }

}
