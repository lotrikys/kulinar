package ua.org.lotrik.kulinar.Models.Recepts;

/**
 * Created by lotrik on 09.12.2015.
 */
public class Nodes {
    private String timestamp;

    private Body body;

    private String title;

    private String nid;

    private String url;

    private String first_anons;

    private String img_anons;

    private String img_anons_fish;

    private String meets_anons;

    private String snacks_anons;

    private String desserts_anons;

    private String salads_anons;

    private String drinks_anons;

    public String getMeets_anons() {
        return meets_anons;
    }

    public void setMeets_anons(String meets_anons) {
        this.meets_anons = meets_anons;
    }

    public String getImg_anons_fish() {
        return img_anons_fish;
    }

    public void setImg_anons_fish(String img_anons_fish) {
        this.img_anons_fish = img_anons_fish;
    }

    public String getImg_anons() {
        return img_anons;
    }

    public void setImg_anons(String img_anons) {
        this.img_anons = img_anons;
    }

    public String getDesserts_anons() {
        return desserts_anons;
    }

    public String getDrinks_anons() {
        return drinks_anons;
    }

    public String getSalads_anons() {
        return salads_anons;
    }

    public String getFirst_anons() {
        return first_anons;
    }

    public void setFirst_anons(String first_anons) {
        this.first_anons = first_anons;
    }

    public void setDesserts_anons(String desserts_anons) {
        this.desserts_anons = desserts_anons;
    }

    public void setSalads_anons(String salads_anons) {
        this.salads_anons = salads_anons;
    }

    public void setDrinks_anons(String drinks_anons) {
        this.drinks_anons = drinks_anons;
    }

    public String getSnacks_anons() {
        return snacks_anons;
    }

    public void setSnacks_anons(String snacks_anons) {
        this.snacks_anons = snacks_anons;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String tnid;

    private String type;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ClassPojo [timestamp = " + timestamp + ", body = " + body + ", display_name = " + ", title = " + title + ", snacks_menu = " + ", nid = " + nid + ", snacks_img = " + ", snacks_anons = " + ", tnid = " + tnid + ", type = " + type + "]";
    }
}
